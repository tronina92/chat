package chat.client;

import chat.network.TCPConnection;
import chat.network.TCPConnectionListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ChatWindow extends JFrame implements TCPConnectionListener {

    private JPanel mainPanel;
    private JPanel inputPanel;
    private JButton sendButton;
    private JTextField inputField;
    private JTextArea log;
    private JPanel nicknamePanel;
    private JLabel fillNickname;
    private JButton sendNickname;
    private JTextField fieldNickname;

    private static final String IP_ADDR = "127.0.0.1";
    private static final int PORT = 8189;
    private TCPConnection connection;

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("ChatWindow");
                frame.setContentPane(new ChatWindow().mainPanel);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setSize(600,300);
                frame.setVisible(true);
            }

        });
    }

    public ChatWindow() {
        super("ChatWindow");
        inputField.setEnabled(false);
        sendButton.setEnabled(false);

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = inputField.getText();
                if (msg.equals("")) return;
                inputField.setText("");
                connection.sendString(msg);
            }
        });

        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = inputField.getText();
                if (msg.equals("")) return;
                inputField.setText("");
                connection.sendString(msg);
            }
        });

        sendNickname.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nickname = fieldNickname.getText();
                sendNickname.setEnabled(false);
                fieldNickname.setEditable(false);
                fieldNickname.setEnabled(false);
                inputField.setEnabled(true);
                sendButton.setEnabled(true);
                inputField.requestFocus();
                try {
                    connection = new TCPConnection(ChatWindow.this, IP_ADDR, PORT);
                    connection.setNick(nickname);
                } catch (IOException ex) {
                    printMessage("Connection Exception " + ex);
                }
            }
        });

        fieldNickname.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nickname = fieldNickname.getText();
                sendNickname.setEnabled(false);
                fieldNickname.setEditable(false);
                fieldNickname.setEnabled(false);
                inputField.setEnabled(true);
                sendButton.setEnabled(true);
                inputField.requestFocus();
                try {
                    connection = new TCPConnection(ChatWindow.this, IP_ADDR, PORT);
                    connection.setNick(nickname);
                } catch (IOException ex) {
                    printMessage("Connection Exception " + ex);
                }
            }
        });
    }

    private synchronized void printMessage(String msg){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(msg + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            }
        });

    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMessage("Connection ready");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMessage("Connection close");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        printMessage("Connection Exception " + e);
    }
}
