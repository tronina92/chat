package chat.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

//класс, описывающий соединение, умеющее принимать входящие сообщения и отправлять сообщения

public class TCPConnection {

    private String nick = "";
    private final Socket socket;
    private final Thread rxThread; //поток, который слушает входящие соединения
    private final BufferedReader in;
    private final BufferedWriter out;
    private final TCPConnectionListener eventListener; //слушатель событий

    //конструктор для случая, когда сокет нужно создать
    public TCPConnection(TCPConnectionListener eventListener, String ipAddr, int port) throws IOException {
        this(eventListener, new Socket(ipAddr, port));
    }

    //конструктор для случая, когда сокет уже создан
    public TCPConnection(TCPConnectionListener eventListener, Socket socket) throws IOException {

        this.eventListener = eventListener;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));

        rxThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    eventListener.onConnectionReady(TCPConnection.this);
                    while (!rxThread.isInterrupted()){
                        eventListener.onReceiveString(TCPConnection.this, in.readLine());
                    }
                } catch (IOException e) {
                    eventListener.onException(TCPConnection.this, e);
                } finally {
                    eventListener.onDisconnect(TCPConnection.this);
                }
            }
        });
        rxThread.start();
    }

    public synchronized void sendString(String value) { //метод для отправки сообщения
        try {
            if (getNick()=="") out.write(value + "\r\n");
            else out.write(getNick() + ": " + value + "\r\n");
            out.flush();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
            disconnect();
        }
    }

    public synchronized void disconnect(){ //метод для разрыва соединения
        rxThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
        }
    }

    public String toString() {
        return "TCPConnection: " + socket.getInetAddress() + ": " + socket.getPort();
    }

    public synchronized void setNick(String n){
        nick = n;
    }

    public synchronized String getNick() {
        return nick;
    }
}
